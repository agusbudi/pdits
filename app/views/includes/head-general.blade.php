<title>PDT ITS</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<link rel="shortcut icon" href="{{ URL::to('favicon.ico') }}" type="image/x-icon">
<link href="{{ URL::to('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('assets/css/stmt-style.css') }}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::to('assets/js/jquery-1.11.1.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::to('assets/js/bootstrap.min.js')}}"></script>